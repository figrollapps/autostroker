﻿using System;
using System.IO;
using System.Linq;
using System.Media;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Figroll.AutoTrainer
{
    internal static class Program
    {
        private static AutoTrainerConfiguration _configuration;
        private static SoundPlayer _strokeSound;
        private static string[] _captions;
        private static readonly Random Randomiser = new Random(DateTime.Now.Millisecond);
        private static int _numberOfExcercisesRequired;
        private static int _credits;

        [STAThread]
        private static void Main(string[] args)
        {
            try
            {
                Configure();
                StartTraining();
                Train();
                EndTraining();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        private static void Configure()
        {
            LoadConfiguration();

            _captions = File.ReadAllLines("captions.txt");
            _strokeSound = new SoundPlayer("53403__calaudio__wood-block.wav");
            _numberOfExcercisesRequired = Randomiser.Next(_configuration.MinExercises, _configuration.MaxExercises);
        }

        private static void LoadConfiguration()
        {
            try
            {
                var serializer = new XmlSerializer(typeof (AutoTrainerConfiguration));
                using (TextReader reader = new StreamReader("config.xml"))
                {
                    _configuration = (AutoTrainerConfiguration) serializer.Deserialize(reader);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error reading config.xml defaults will be used");
                Console.WriteLine(e.Message);

                _configuration = new AutoTrainerConfiguration();
            }
        }

        private static void StartTraining()
        {
            Say("You're training will begin shortly. Just one moment while I find your records.");
            RandomPauseSeconds(12, 20);
            Say("Oh dear. This is bad. Very bad. I can see why they sent you to me.");
            Thread.Sleep(10000);
            Say("Lots of work ahead of us I think. Get ready to stroke and remember. Do not, under any circumstances, cum.");
            Thread.Sleep(12000);
        }

        private static void Say(string caption)
        {
            if (String.IsNullOrEmpty(caption))
                return;

            Console.WriteLine(caption);
            Clipboard.SetText(caption);
        }

        private static void RandomPauseSeconds(int minSeconds, int maxSeconds)
        {
            Thread.Sleep(Randomiser.Next(minSeconds*1000, maxSeconds*1000));
        }

        private static void Train()
        {
            for (var excerciseNumber = 1; excerciseNumber <= _numberOfExcercisesRequired; excerciseNumber++)
            {
                Exercise(excerciseNumber);
                Rest(_configuration.MinimumRestSeconds, _configuration.MaximumRestSeconds);

                _credits += 5;
            }
        }

        private static void Exercise(int exerciseNumber)
        {
            var count = GenerateRandomStrokeCount();
            count = RoundTo(count, _configuration.CountStep);

            var bpm = GenerateCountAppropriateBPM(count);

            bpm = RoundTo(bpm, _configuration.BpmStep);
            var milliseconds = (int) (1000.0/(bpm/60.0));

            Say("Exercise " + exerciseNumber + ". " + count + " strokes at " + bpm);
            Thread.Sleep(7000);
            for (var number = 0; number < count - 1; number++)
            {
                _credits++;
                _strokeSound.Play();
                Thread.Sleep(milliseconds);
            }

            Say("Stop. Hands off.");
            Thread.Sleep(1000);
            _credits += 5;
        }

        private static int GenerateRandomStrokeCount()
        {
            return Randomiser.Next(_configuration.MinStrokeCount, _configuration.MaxStrokeCount);
        }

        private static int RoundTo(int count, int step)
        {
            if (step == 0)
                return count;

            return (int) Math.Round((count/(double) step))*step;
        }

        private static int GenerateCountAppropriateBPM(int count)
        {
            int bpm;

            if (count < _configuration.PreferSlowBeatsUnderCount)
            {
                bpm = GenerateSlowBPM();
            }
            else if (count > _configuration.PreferFastBeatsOverCount)
            {
                bpm = GenerateFastBPM();
            }
            else
            {
                bpm = GenerateRandomBPM();
            }

            return bpm;
        }

        private static int GenerateFastBPM()
        {
            return Randomiser.Next((int) (_configuration.MaxBpm/2.0), _configuration.MaxBpm);
        }

        private static int GenerateSlowBPM()
        {
            return Randomiser.Next(_configuration.MinBpm,
                Math.Min((int) (_configuration.MaxBpm/3.0), _configuration.MinBpm));
        }

        private static int GenerateRandomBPM()
        {
            return Randomiser.Next(_configuration.MinBpm, _configuration.MaxBpm);
        }

        private static void Rest(int minRest, int maxRest)
        {
            var restTimeMs = Randomiser.Next(minRest*1000, maxRest*1000);
            if (_configuration.TauntSayTime != 0)
            {
                Thread.Sleep((int) (restTimeMs/2.0));
                Taunt();
                Thread.Sleep((int) (restTimeMs/2.0));
            }
            else
            {
                Thread.Sleep(restTimeMs);
            }
        }

        private static void Taunt()
        {
            Say(_captions.ElementAt(Randomiser.Next(0, _captions.Count() - 1)));
            Thread.Sleep(_configuration.TauntSayTime*1000);
            _credits += 5;
        }

        private static void EndTraining()
        {
            Say("Training is over for today. You earned " + _credits + " credits.");
            Thread.Sleep(1000);
            Say("You need a total of ten thousands credits to complete training.");
        }
    }
}