﻿namespace Figroll.AutoTrainer
{
    public class AutoTrainerConfiguration
    {
        public AutoTrainerConfiguration()
        {
            MinStrokeCount = 10;
            MaxStrokeCount = 50;
            CountStep = 5;
            MinExercises = 10;
            MaxExercises = 50;
            MinBpm = 45;
            MaxBpm = 180;
            BpmStep = 15;
            MinimumRestSeconds = 2;
            MaximumRestSeconds = 2;
            TauntSayTime = 8;
            PreferSlowBeatsUnderCount = 21;
            PreferFastBeatsOverCount = 41;
        }

        public int MinExercises { get; set; }
        public int MaxExercises { get; set; }
        public int MinStrokeCount { get; set; }
        public int MaxStrokeCount { get; set; }
        public int CountStep { get; set; }
        public int MinBpm { get; set; }
        public int MaxBpm { get; set; }
        public int BpmStep { get; set; }
        public int MinimumRestSeconds { get; set; }
        public int MaximumRestSeconds { get; set; }
        public int PreferSlowBeatsUnderCount { get; set; }
        public int PreferFastBeatsOverCount { get; set; }
        public int TauntSayTime { get; set; }
    }
}